/* 
 Теоретичні питання
 1. Як можна сторити функцію та як ми можемо її викликати? 
 Створити за допомогую funtion та надати їй імя (якщо Function declaration) або записати в зміну(якщо Function Expression - імя надавати необязково). Виклакається фуккція: имя або зміна а після неї дужки - name()

 2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
Використовуються для повернення значення функції, тобто для того шоб отримати результат виконання фунції

 3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Аргументи це дані, які передаються при виклику функції. Параметри - це аргументи, які були передані функції при виклику самої функції.

 4. Як передати функцію аргументом в іншу функцію? 

 Такі функції називаються CALLBACK - передаються аргументом функції, яку викликають.

 Практичні завдання
 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
 
 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.

3. Опціонально. Завдання: 
Реалізувати функцію підрахунку факторіалу числа. 
Технічні вимоги:
- Отримати за допомогою модального вікна браузера число, яке введе користувач.
- За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
- Використовувати синтаксис ES6 для роботи зі змінними та функціями.
*/

// ЗАДАЧІ №1

// let number1 = prompt('Введіть перше число')
// let number2 = prompt('Введіть друге число')
// while (true) {
//   if (number1 !== null && number1 !== '' && !isNaN(number1)) {
//     break
//   } else {
//     number1 = prompt('Введіть перше число ще раз')

//     if (number1 !== null && number1 !== '' && !isNaN(number1)) {
//       break
//     }
//   }
// }

// while (true) {
//   if (number2 !== null && number2 !== '' && !isNaN(number2) && +number2 !== 0) {
//     break
//   } else {
//     number2 = prompt('Введіть другое число ще раз')
//     if (
//       number2 !== null &&
//       number2 !== '' &&
//       !isNaN(number2) &&
//       +number2 !== 0
//     ) {
//       break
//     }
//   }
// }

// function getFraction(number1, number2) {
//   let fraction = number1 / number2
//   return fraction
// }

// console.log(
//   `Частка чисел ${number1} та ${number2} = `,
//   getFraction(number1, number2)
// )

// ЗАДАЧІ №2

let number1 = prompt('Введіть перше число')
let number2 = prompt('Введіть друге число')
let action = prompt('Оберить математичну дію ')

while (true) {
  if (number1 !== null && number1 !== '' && !isNaN(number1)) {
    break
  } else {
    number1 = prompt('Введіть перше число ще раз')

    if (number1 !== null && number1 !== '' && !isNaN(number1)) {
      break
    }
  }
}

while (true) {
  if (number2 !== null && number2 !== '' && !isNaN(number2)) {
    break
  } else {
    number2 = prompt('Введіть другое число ще раз')
    if (number2 !== null && number2 !== '' && !isNaN(number2)) {
      break
    }
  }
}

function getAction(number1, number2, action) {
  let result
  switch (action) {
    case `+`:
      result = +number1 + +number2
      break
    case `-`:
      result = number1 - number2
      break

    case `/`:
      result = number1 / number2
      break
    case `*`:
      result = number1 * number2
      break
    default:
      alert('Такої операції не існує')
  }

  return result
}

if (+number2 === 0 && action === '/') {
  console.log('ПОМИЛКА. Ділення на "0" заборонено')
} else if (getAction(number1, number2, action) === undefined) {
  console.log('Вибрано некоректну математичну дію')
} else {
  console.log(
    `Результата дії "${action}" чисел ${number1} та ${number2} = `,
    getAction(number1, number2, action)
  )
}

//Задача 3

// let number = prompt('Введіть число')
// while (true) {
//   if (number !== null && number !== '' && !isNaN(number)) {
//     break
//   } else {
//     number = prompt('Введіть число ще раз')
//     if (number !== null && number !== '' && !isNaN(number)) {
//       break
//     }
//   }
// }
// function getFactorial(number) {
//   let result = 1
//   for (let i = 1; i <= number; ++i) {
//     result = result * i
//   }
//   return result
// }
// alert(`Факторіал ${number} = ` + getFactorial(number))
